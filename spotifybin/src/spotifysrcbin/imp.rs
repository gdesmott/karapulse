// Copyright (C) 2021 Guillaume Desmottes <guillaume@desmottes.be>
//
// This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
// If a copy of the MPL was not distributed with this file, You can obtain one at
// <https://mozilla.org/MPL/2.0/>.
//
// SPDX-License-Identifier: MPL-2.0

use std::sync::{Arc, LazyLock, Mutex};

use gst::glib;
use gst::{prelude::*, subclass::prelude::*};
use gstspotify::common::Settings;

static CAT: LazyLock<gst::DebugCategory> = LazyLock::new(|| {
    gst::DebugCategory::new(
        "karapulsespotifysrcbin",
        gst::DebugColorFlags::empty(),
        Some("Spotify source"),
    )
});

pub struct SpotifySrcBin {
    audio: gst::Element,
    audio_decodebin: gst::Element,
    video: gst::Element,
    video_caps: gst::Element,
    lyrics: gst::Element,
    textahead: gst::Element,

    settings: Mutex<Settings>,
}

#[glib::object_subclass]
impl ObjectSubclass for SpotifySrcBin {
    const NAME: &'static str = "KarapulseSpotifySrcBin";
    type Type = super::SpotifySrcBin;
    type ParentType = gst::Bin;
    type Interfaces = (gst::URIHandler,);

    fn with_class(_klass: &Self::Class) -> Self {
        let audio = gst::ElementFactory::make("spotifyaudiosrc")
            .build()
            .unwrap();
        let audio_decodebin = gst::ElementFactory::make("decodebin3").build().unwrap();

        let video = gst::ElementFactory::make("videotestsrc").build().unwrap();
        video.set_property_from_str("pattern", "black");
        let video_caps = gst::ElementFactory::make("capsfilter").build().unwrap();
        video_caps.set_property(
            "caps",
            gst::Caps::builder("video/x-raw")
                .field("width", 1920)
                .field("height", 1080)
                .build(),
        );

        let lyrics = gst::ElementFactory::make("spotifylyricssrc")
            .build()
            .unwrap();
        let textahead = gst::ElementFactory::make("textahead").build().unwrap();
        textahead.set_property("n-ahead", 5u32);
        textahead.set_property("buffer-start-segment", true);
        textahead.set_property("n-previous", 2u32);

        let video_clone = video.clone();
        lyrics.connect_notify(Some("background-color"), move |lyrics, _psec| {
            let bg_color = lyrics.property::<u32>("background-color");
            video_clone.set_property("background-color", bg_color);
        });

        fn argb_to_pango(argb: u32) -> String {
            let rgb = argb & 0x00ffffff;
            format!("#{:0>6x}", rgb)
        }

        const FONT: &str = "font_family=\"Open Sans\" font_weight=\"bold\"";

        let textahead_clone = textahead.clone();
        lyrics.connect_notify(Some("text-color"), move |lyrics, _psec| {
            let color = lyrics.property::<u32>("text-color");
            let mut attr = format!(
                "{} size=\"smaller\" color=\"{}\"",
                FONT,
                argb_to_pango(color),
            );
            textahead_clone.set_property("ahead-attributes", &attr);

            // use the same color as ahead for previous lyrics but with 50% alpha
            attr.push_str(" alpha=\"50%\"");
            textahead_clone.set_property("previous-attributes", &attr);
        });

        let textahead_clone = textahead.clone();
        lyrics.connect_notify(Some("highlight-text-color"), move |lyrics, _psec| {
            let color = lyrics.property::<u32>("highlight-text-color");
            let attr = format!(
                "{} size=\"larger\" color=\"{}\"",
                FONT,
                argb_to_pango(color),
            );
            textahead_clone.set_property("current-attributes", attr);
        });

        Self {
            audio,
            audio_decodebin,
            video,
            video_caps,
            lyrics,
            textahead,
            settings: Mutex::new(Settings::default()),
        }
    }
}

impl ObjectImpl for SpotifySrcBin {
    fn properties() -> &'static [glib::ParamSpec] {
        static PROPERTIES: LazyLock<Vec<glib::ParamSpec>> = LazyLock::new(Settings::properties);
        PROPERTIES.as_ref()
    }

    fn set_property(&self, _id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
        gst::debug!(
            CAT,
            imp = self,
            "set property {}: {:?}",
            pspec.name(),
            value
        );

        let mut settings = self.settings.lock().unwrap();
        settings.set_property(value, pspec);

        self.audio.set_property_from_value(pspec.name(), value);
        self.lyrics.set_property_from_value(pspec.name(), value);
    }

    fn property(&self, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
        let settings = self.settings.lock().unwrap();
        settings.property(pspec)
    }

    fn constructed(&self) {
        self.parent_constructed();

        // audio
        self.obj().add(&self.audio).unwrap();
        self.obj().add(&self.audio_decodebin).unwrap();
        self.audio.link(&self.audio_decodebin).unwrap();

        // urisourcebin does not play nice if we mix raw/encoded pads,
        // see https://gitlab.freedesktop.org/gstreamer/gstreamer/-/issues/1029
        // and/or static and sometimes pads. So decode audio and then add the other
        // pads as well.
        let element_weak = self.obj().downgrade();
        let video = self.video.clone();
        let video_caps = self.video_caps.clone();
        let lyrics = self.lyrics.clone();
        let textahead = self.textahead.clone();

        self.audio_decodebin
            .connect_pad_added(move |_decodebin, pad| {
                // ghost decodebin pads
                let element = match element_weak.upgrade() {
                    Some(element) => element,
                    None => return,
                };

                // set video eos when both audio and lyrics are eos
                let pending_eos = Arc::new(Mutex::new(2));

                // handle audio pad
                let templ = element.pad_template("audio_src").unwrap();
                let audio_src = gst::GhostPad::builder_from_template_with_target(&templ, pad)
                    .unwrap()
                    .name("audio_src")
                    .build();
                audio_src.set_active(true).unwrap();
                element.add_pad(&audio_src).unwrap();

                let pending_eos_clone = pending_eos.clone();
                let video_clone = video.clone();
                pad.add_probe(gst::PadProbeType::EVENT_DOWNSTREAM, move |_pad, info| {
                    match info.data {
                        Some(gst::PadProbeData::Event(ref ev))
                            if ev.type_() == gst::EventType::Eos =>
                        {
                            let mut pending_eos = pending_eos_clone.lock().unwrap();
                            *pending_eos -= 1;
                            if *pending_eos == 0 {
                                video_clone.send_event(gst::event::Eos::new());
                            }
                        }
                        _ => {}
                    }

                    gst::PadProbeReturn::Pass
                });

                // video
                element.add(&video).unwrap();
                element.add(&video_caps).unwrap();
                video.link(&video_caps).unwrap();

                let templ = element.pad_template("video_src").unwrap();
                let src = video_caps.static_pad("src").unwrap();
                let video_src = gst::GhostPad::builder_from_template_with_target(&templ, &src)
                    .unwrap()
                    .name("video_src")
                    .build();
                video_src.set_active(true).unwrap();
                element.add_pad(&video_src).unwrap();
                video.sync_state_with_parent().unwrap();
                video_caps.sync_state_with_parent().unwrap();

                // lyrics
                element.add(&lyrics).unwrap();
                element.add(&textahead).unwrap();
                lyrics.link(&textahead).unwrap();

                let templ = element.pad_template("lyrics_src").unwrap();
                let src = textahead.static_pad("src").unwrap();
                let lyrics_src = gst::GhostPad::builder_from_template_with_target(&templ, &src)
                    .unwrap()
                    .name("lyrics_src")
                    .build();
                lyrics_src.set_active(true).unwrap();
                element.add_pad(&lyrics_src).unwrap();

                let video_clone = video.clone();
                lyrics_src.add_probe(gst::PadProbeType::EVENT_DOWNSTREAM, move |_pad, info| {
                    match info.data {
                        Some(gst::PadProbeData::Event(ref ev))
                            if ev.type_() == gst::EventType::Eos =>
                        {
                            let mut pending_eos = pending_eos.lock().unwrap();
                            *pending_eos -= 1;
                            if *pending_eos == 0 {
                                video_clone.send_event(gst::event::Eos::new());
                            }
                        }
                        _ => {}
                    }

                    gst::PadProbeReturn::Pass
                });

                textahead.sync_state_with_parent().unwrap();
                lyrics.sync_state_with_parent().unwrap();
            });
    }
}

impl GstObjectImpl for SpotifySrcBin {}

impl ElementImpl for SpotifySrcBin {
    fn metadata() -> Option<&'static gst::subclass::ElementMetadata> {
        static ELEMENT_METADATA: LazyLock<gst::subclass::ElementMetadata> = LazyLock::new(|| {
            gst::subclass::ElementMetadata::new(
                "Spotify source",
                "Source/Audio",
                "Spotify source",
                "Guillaume Desmottes <guillaume@desmottes.be>",
            )
        });

        Some(&*ELEMENT_METADATA)
    }

    fn pad_templates() -> &'static [gst::PadTemplate] {
        static PAD_TEMPLATES: LazyLock<Vec<gst::PadTemplate>> = LazyLock::new(|| {
            let caps = gst::Caps::builder("audio/x-raw").build();
            let audio_src_pad_template = gst::PadTemplate::new(
                "audio_src",
                gst::PadDirection::Src,
                gst::PadPresence::Sometimes,
                &caps,
            )
            .unwrap();

            let caps = gst::Caps::builder("video/x-raw").build();
            let video_src_pad_template = gst::PadTemplate::new(
                "video_src",
                gst::PadDirection::Src,
                gst::PadPresence::Sometimes,
                &caps,
            )
            .unwrap();

            let caps = gst::Caps::builder("text/x-raw")
                .field("format", "pango-markup")
                .build();
            let text_src_pad_template = gst::PadTemplate::new(
                "lyrics_src",
                gst::PadDirection::Src,
                gst::PadPresence::Sometimes,
                &caps,
            )
            .unwrap();

            vec![
                audio_src_pad_template,
                video_src_pad_template,
                text_src_pad_template,
            ]
        });

        PAD_TEMPLATES.as_ref()
    }
}

impl BinImpl for SpotifySrcBin {
    fn handle_message(&self, message: gst::Message) {
        match message.view() {
            // discard stream messages for our internal decodebin3 so downstream parsebin will take care of those
            gst::MessageView::StreamCollection(_) | gst::MessageView::StreamsSelected(_) => {}
            _ => self.parent_handle_message(message),
        }
    }
}

impl URIHandlerImpl for SpotifySrcBin {
    const URI_TYPE: gst::URIType = gst::URIType::Src;

    fn protocols() -> &'static [&'static str] {
        &["spotify"]
    }

    fn uri(&self) -> Option<String> {
        let settings = self.settings.lock().unwrap();

        if settings.track.is_empty() {
            None
        } else {
            Some(settings.track.clone())
        }
    }

    fn set_uri(&self, uri: &str) -> Result<(), glib::Error> {
        gst::debug!(CAT, imp = self, "set URI: {}", uri);

        let url = url::Url::parse(uri)
            .map_err(|e| glib::Error::new(gst::URIError::BadUri, &format!("{:?}", e)))?;

        // allow to configure auth and cache settings from the URI
        for (key, value) in url.query_pairs() {
            match key.as_ref() {
                "access-token" | "cache-credentials" | "cache-files" => {
                    self.obj().set_property(&key, value.as_ref());
                }
                _ => {
                    gst::warning!(CAT, imp = self, "unsupported query: {}={}", key, value);
                }
            }
        }

        self.obj()
            .set_property("track", format!("{}:{}", url.scheme(), url.path()));

        Ok(())
    }
}
