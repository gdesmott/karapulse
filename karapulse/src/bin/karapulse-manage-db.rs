// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.
use anyhow::Result;
use chrono::Duration;
use env_logger::{Builder, Env};
use itertools::Itertools;
use std::collections::HashMap;
use std::path::{Path, PathBuf};
use structopt::StructOpt;

use karapulse::{common, crawler::Crawler, db::DB, karapulse::Song, settings::Settings, spotify};

#[derive(StructOpt, Debug)]
#[structopt(name = "command")]
enum Command {
    #[structopt(name = "list-songs")]
    ListSongs,
    #[structopt(name = "add-song")]
    AddSong {
        path: PathBuf,
        artist: String,
        title: String,
        length: Option<i32>,
    },
    #[structopt(name = "remove-song")]
    RemoveSong { rowid: i64 },
    #[structopt(name = "prune-songs")]
    PruneSongs,
    #[structopt(name = "search-song")]
    SearchSong { fields: String },
    #[structopt(name = "add-file")]
    AddFile { path: PathBuf },
    #[structopt(name = "add-dir")]
    AddDir { path: PathBuf },
    #[structopt(name = "list-history")]
    ListHistory { user: Option<String> },
    #[structopt(name = "add-history")]
    AddHistory { song: i64, user: String },
    #[structopt(name = "set-history-played")]
    SetHistoryPlayed { history: i64 },
    #[structopt(name = "history-not-played")]
    HistoryNotPlayed,
    #[structopt(name = "purge-history", help = "Remove all entries from the history")]
    PurgeHistory,
    #[structopt(name = "stats")]
    Stats,
}

#[derive(StructOpt)]
#[structopt(name = "manage-db")]
struct Opt {
    #[structopt(
        long = "config",
        short = "c",
        parse(from_os_str),
        help = "Configuration file"
    )]
    config_file: Option<PathBuf>,
    #[structopt(name = "db")]
    path: Option<PathBuf>,
    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(Debug)]
struct Stat {
    played_count: u32,
    played_time: u32,
}

impl Stat {
    fn new() -> Self {
        Self {
            played_count: 0,
            played_time: 0,
        }
    }
}

async fn stats(db: &DB, spotify: &spotify::Spotify) -> Result<()> {
    let mut played = HashMap::new();

    println!("Played songs:");
    for h in db.list_history()? {
        if h.played.is_some() {
            let stat = played.entry(h.user.to_string()).or_insert_with(Stat::new);
            stat.played_count += 1;
            let song = karapulse::karapulse::Song::from_history_song(h.song, spotify).await;
            if let Ok(song) = song {
                stat.played_time += song.duration();
            }
        }
    }

    for (user, stat) in played
        .iter()
        .sorted_by(|(_, a), (_, b)| b.played_count.cmp(&a.played_count))
    {
        let d = Duration::try_seconds(i64::from(stat.played_time)).unwrap();
        println!(
            "  {}: {} ({}h {}m)",
            user,
            stat.played_count,
            d.num_hours(),
            d.num_minutes() % 60
        );
    }

    Ok(())
}

/// Remove all songs which path no longer exists
fn prune_songs(db: &DB) -> Result<()> {
    let songs = db.list_songs()?;

    for song in songs.into_iter() {
        let p = Path::new(&song.path);
        if !p.exists() {
            println!(
                "Remove song {}, path no longer exists {}",
                song.rowid, song.path
            );

            db.remove_song(song.rowid)?;
        }
    }

    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    Builder::from_env(Env::default().default_filter_or("warn")).init();

    karapulse::common::init()?;

    let opt = Opt::from_args();

    let db = DB::new(&opt.path)?;
    let crawler = Crawler::new(&db);

    let settings_path = opt
        .config_file
        .clone()
        .unwrap_or_else(|| crate::common::config_dir_file("config.toml").unwrap());
    let settings = Settings::from_path(&settings_path)?;

    let spotify = spotify::Spotify::new(settings).await;

    match opt.cmd {
        Command::ListSongs => {
            println!("Listing songs:");
            for song in db.list_songs()? {
                println!("{:?}", song);
            }
        }
        Command::AddSong {
            path,
            artist,
            title,
            length,
        } => {
            db.add_song(&path, &artist, &title, length)?;
            println!("Added song");
        }
        Command::RemoveSong { rowid } => {
            db.remove_song(rowid)?;
            println!("Removed song");
        }
        Command::PruneSongs => {
            prune_songs(&db)?;
        }
        Command::SearchSong { fields } => {
            println!("Searching song matching '{}':", fields);
            for song in db.search(&fields)? {
                println!("{:?}", song);
            }
        }
        Command::AddFile { path } => {
            println!("Adding file {}", path.display());
            crawler.add_file(&path)?;
        }
        Command::AddDir { path } => {
            println!("Adding directory {}", path.display());
            crawler.add_dir(&path)?;
        }
        Command::ListHistory { user } => {
            let history = match user {
                Some(user) => {
                    println!("Listing history for {}:", user);
                    db.history_for(&user)?
                }
                None => {
                    println!("Listing history");
                    db.list_history()?
                }
            };

            for h in history {
                let user = h.user.clone();
                if let Ok(song) = Song::from_history_song(h.song, &spotify).await {
                    let info = song.info().unwrap();
                    println!(
                        "{} {} : {} - {}",
                        h.played.unwrap(),
                        user,
                        info.artist,
                        info.title
                    );
                }
            }
        }
        Command::AddHistory { song, user } => {
            let s = db.find_song(song)?;
            let id = db.add_history_db(&s, &user)?;
            println!("Added history: {}", id);
        }
        Command::SetHistoryPlayed { history } => {
            db.set_history_played(history)?;
            println!("History set as played");
        }
        Command::HistoryNotPlayed => {
            println!("Not played history:");
            for h in db.history_not_played()? {
                println!("{:?}", h);
            }
        }
        Command::PurgeHistory => {
            db.purge_history()?;
            println!("History purged");
        }
        Command::Stats => {
            stats(&db, &spotify).await?;
        }
    }

    Ok(())
}
