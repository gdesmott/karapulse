// Copyright (C) 2022 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use std::{
    collections::HashMap,
    path::PathBuf,
    sync::{Arc, Mutex, RwLock},
};

use anyhow::{bail, Result};
use librespot::{
    core::{cache::Cache, config::SessionConfig, session::Session, spotify_id::SpotifyId},
    metadata::Metadata,
};
use rspotify::{
    model::{FullTrack, SearchResult, SearchType, TrackId},
    prelude::*,
    Credentials,
};

use crate::settings::Settings;

// 50 is the maximum number of results returned by the Spotify API, see
// https://developer.spotify.com/documentation/web-api/reference/#/operations/search
// We'll have to implement pagination if we want to return more results.
const SEARCH_LIMIT: u32 = 50;

const PREFERRED_COVER_WIDTH: u32 = 300;

/// How many cover requests are allowed per minute.
/// We deliberately hit lower than what seems to be Spotify's limit (180) to keep room for the search requests which are not limited
const MAX_COVER_REQUESTS_PER_MINUTE: u32 = 90;

/// Number of tracks send together when retrieving songs
const N_TRACKS_PER_REQUEST: usize = 50;

/// Karapulse's Spotify client ID, from https://developer.spotify.com/dashboard
const CLIENT_ID: &str = "90cc7bcd3a2c48b4a216e83462fbf54c";

#[derive(Clone)]
enum State {
    NotAuth {
        rspotify_client: rspotify::AuthCodePkceSpotify,
        auth_url: String,
    },
    Auth {
        rspotify_client: rspotify::AuthCodePkceSpotify,
        librespot_session: Session,
    },
}

impl State {
    fn not_auth() -> Self {
        let mut rspotify_client = create_rspotify_client(None);
        let auth_url = rspotify_client.get_authorize_url(None).unwrap();
        State::NotAuth {
            rspotify_client,
            auth_url,
        }
    }

    fn clients(self) -> Option<(rspotify::AuthCodePkceSpotify, Session)> {
        match self {
            State::NotAuth { .. } => None,
            State::Auth {
                rspotify_client,
                librespot_session,
            } => Some((rspotify_client, librespot_session)),
        }
    }
}

#[derive(Clone)]
pub struct Spotify {
    settings: Settings,
    /// only set when Spotify is configured
    state: Arc<Mutex<State>>,

    // search caches
    // track id -> true/false if the lyrics are available and synced
    cache_has_lyrics: Arc<RwLock<HashMap<SpotifyId, bool>>>,
    cache_song: Arc<RwLock<HashMap<String, Song>>>,
    // "artist title" -> cover URL
    cache_cover: Arc<RwLock<HashMap<String, Option<String>>>>,

    /// limiter to no blow our API calls limit just by fetching lots of covers.
    cover_limiter: Arc<
        governor::RateLimiter<
            governor::state::NotKeyed,
            governor::state::InMemoryState,
            governor::clock::DefaultClock,
            governor::middleware::NoOpMiddleware,
        >,
    >,
}

pub fn cache_dir() -> anyhow::Result<PathBuf> {
    let spotify_cache = crate::common::cache_dir_directory("spotify")?;
    Ok(spotify_cache)
}

async fn create_librespot_session(access_token: String) -> anyhow::Result<Session> {
    let spotify_cache = crate::spotify::cache_dir()?;
    let spotify_cache = spotify_cache.to_str().unwrap();
    let cache = Cache::new(Some(spotify_cache), None, Some(spotify_cache), None)?;

    let credentials = match cache.credentials() {
        Some(cached_cred) => cached_cred,
        None => {
            let cred = librespot::discovery::Credentials::with_access_token(access_token);
            cache.save_credentials(&cred);
            cred
        }
    };

    let session = Session::new(SessionConfig::default(), Some(cache));
    session.connect(credentials, true).await?;

    Ok(session)
}

async fn access_token_from_rspotify(client: &rspotify::AuthCodePkceSpotify) -> Option<String> {
    let token = client.get_token();
    let token = token.lock().await.unwrap();
    token.as_ref().map(|token| token.access_token.clone())
}

async fn create_state() -> State {
    let rspotify_cache = rspotify_cache_path();
    let token = rspotify::Token::from_cache(rspotify_cache);
    if let Err(err) = &token {
        debug!("failed to retrieve token from cache: {err}");
    }
    let token = token.ok();
    let expired = token
        .as_ref()
        .map(|token| token.is_expired())
        .unwrap_or_default();
    let rspotify_client = create_rspotify_client(token);

    if expired {
        debug!("token expired");
        match rspotify_client.refresh_token().await {
            Ok(_) => {
                debug!("token refreshed");
            }
            Err(err) => {
                warn!("failed to refresh token: {err}");
                return State::not_auth();
            }
        }
    }

    match access_token_from_rspotify(&rspotify_client).await {
        Some(access_token) => {
            debug!("token retrieved from cache");
            // librespot connection, used to check if tracks have synced lyrics
            match create_librespot_session(access_token).await {
                Ok(librespot_session) => State::Auth {
                    rspotify_client,
                    librespot_session,
                },
                Err(err) => {
                    warn!("failed to connect librespot: {err}");

                    State::not_auth()
                }
            }
        }
        None => State::not_auth(),
    }
}

fn rspotify_cache_path() -> PathBuf {
    let mut cache_path = crate::spotify::cache_dir().unwrap();
    cache_path.push("rspotify_token_cache.json");
    cache_path
}

fn create_rspotify_client(token: Option<rspotify::Token>) -> rspotify::AuthCodePkceSpotify {
    let creds = Credentials::new_pkce(CLIENT_ID);
    let oauth = rspotify::OAuth {
        redirect_uri: "http://127.0.0.1:1848/api/spotify_auth".to_string(),
        scopes: rspotify::scopes!("streaming"),
        proxies: None,
        ..Default::default()
    };
    let config = rspotify::Config {
        cache_path: rspotify_cache_path(),
        token_cached: true,
        token_refreshing: true,
        ..Default::default()
    };

    match token {
        Some(token) => {
            // FIXME: need https://github.com/ramsayleung/rspotify/pull/512 released
            //rspotify::AuthCodePkceSpotify::from_token_with_config(token, creds, oauth, config)
            let mut client = rspotify::AuthCodePkceSpotify::with_config(creds, oauth, config);
            client.token = Arc::new(rspotify::sync::Mutex::new(Some(token)));
            client
        }
        None => rspotify::AuthCodePkceSpotify::with_config(creds, oauth, config),
    }
}

impl Spotify {
    pub async fn new(settings: Settings) -> Self {
        let cover_limiter = governor::RateLimiter::direct(governor::Quota::per_minute(
            nonzero_ext::nonzero!(MAX_COVER_REQUESTS_PER_MINUTE),
        ));

        let state = create_state().await;
        match &state {
            State::Auth { .. } => {
                info!("successfully logged to Spotify");
            }
            State::NotAuth { auth_url, .. } => {
                println!("Use this link to link Karapulse with your Spotify account:\n{auth_url}");
            }
        }

        Spotify {
            settings,
            state: Arc::new(Mutex::new(state)),
            cache_has_lyrics: Arc::new(RwLock::new(HashMap::new())),
            cache_song: Arc::new(RwLock::new(HashMap::new())),
            cache_cover: Arc::new(RwLock::new(HashMap::new())),
            cover_limiter: Arc::new(cover_limiter),
        }
    }

    /// Pass the auth code received from Spotify for authentication.
    pub async fn auth_token(&self, code: &str) -> Result<()> {
        let state = self.state.lock().unwrap().clone();

        match state {
            State::NotAuth {
                rspotify_client, ..
            } => {
                rspotify_client.request_token(code).await?;

                let Some(access_token) = access_token_from_rspotify(&rspotify_client).await else {
                    bail!("no access token from reply code?!");
                };
                info!("Spotify account linked");
                let librespot_session = create_librespot_session(access_token).await?;

                let mut state = self.state.lock().unwrap();
                *state = State::Auth {
                    rspotify_client,
                    librespot_session,
                };

                Ok(())
            }
            State::Auth { .. } => {
                bail!("already linked with Spotify");
            }
        }
    }

    pub async fn search(&self, fields: &str) -> Result<Vec<Song>> {
        let state = self.state.lock().unwrap().clone();
        let Some((rspotify_client, librespot_session)) = state.clients() else {
            return Ok(vec![]);
        };

        let res = rspotify_client
            .search(
                fields,
                SearchType::Track,
                None,
                None,
                Some(SEARCH_LIMIT),
                None,
            )
            .await;

        let res = match res {
            Ok(res) => res,
            Err(err) => {
                warn!("failed to search from Spotify: {:?}", err);
                return Err(err.into());
            }
        };

        match res {
            SearchResult::Tracks(res) => {
                let tracks = res
                    .items
                    .into_iter()
                    .filter(|track| track.id.is_some())
                    .filter(|track| !track.artists.is_empty())
                    .filter(|track| track.is_playable.unwrap_or(true));

                let session = librespot_session.clone();
                let cache = self.cache_has_lyrics.clone();

                let mut futures = vec![];

                for track in tracks {
                    let fut = async {
                        let session = session.clone();
                        let track_id =
                            SpotifyId::from_uri(&track.id.as_ref().unwrap().uri()).unwrap();
                        let cache = cache.clone();

                        let has_lyrics_from_cache = {
                            let cache_guard = cache.read().unwrap();
                            cache_guard.get(&track_id) == Some(&true)
                        };

                        let has_lyrics = if has_lyrics_from_cache {
                            true
                        } else {
                            // Spotify now seems to rate limit this API so use the metadata instead but that means we can no longer check if the lyrics are synced. :\
                            /*
                            // query Spotify to check if we have synced lyrics
                                                       let has_lyrics = librespot::metadata::Lyrics::get(&session, &track_id)
                                                           .await
                                                           .is_ok_and(|lyrics| {
                                                               lyrics.lyrics.sync_type
                                                                   == librespot::metadata::lyrics::SyncType::LineSynced
                                                           });
                            */

                            let has_lyrics = librespot::metadata::Track::get(&session, &track_id)
                                .await
                                .map(|track| track.has_lyrics)
                                .unwrap_or_default();

                            {
                                let mut cache_guard = cache.write().unwrap();
                                cache_guard.insert(track_id, has_lyrics);
                            }

                            has_lyrics
                        };

                        if has_lyrics {
                            Some(self.track_to_song(track))
                        } else {
                            None
                        }
                    };

                    futures.push(fut);
                }

                let songs = futures::future::join_all(futures)
                    .await
                    .into_iter()
                    .flatten()
                    .collect();

                Ok(songs)
            }
            _ => bail!("did not return tracks result"),
        }
    }

    pub async fn song_from_id(&self, id: &str) -> Result<Song> {
        let cached_song = {
            let cache_guard = self.cache_song.read().unwrap();
            cache_guard.get(id).cloned()
        };

        if let Some(cached_song) = cached_song {
            return Ok(cached_song);
        }

        let state = self.state.lock().unwrap().clone();
        let Some((rspotify_client, _librespot_session)) = state.clients() else {
            bail!("no connected to Spotify");
        };

        let track_id = TrackId::from_id(id)?;
        let res = rspotify_client.track(track_id, None).await;

        let track = match res {
            Ok(res) => res,
            Err(err) => {
                warn!("failed to retrieve song from Spotify: {:?}", err);
                return Err(err.into());
            }
        };

        let song = self.track_to_song(track);

        {
            let mut cache_guard = self.cache_song.write().unwrap();
            cache_guard.insert(id.to_string(), song.clone());
        }

        Ok(song)
    }

    pub async fn songs_from_id(&self, ids: Vec<String>) -> Result<Vec<Song>> {
        let state = self.state.lock().unwrap().clone();
        let Some((rspotify_client, _librespot_session)) = state.clients() else {
            bail!("no connected to Spotify");
        };

        let tracks_id: Result<Vec<_>, _> = ids.into_iter().map(TrackId::from_id).collect();
        let tracks_id = tracks_id?;
        let mut songs = vec![];

        // we can't send to many tracks to Spotify in one shot
        for chunk in tracks_id.chunks(N_TRACKS_PER_REQUEST) {
            let tracks_id = Vec::from(chunk);
            let res = rspotify_client.tracks(tracks_id, None).await;

            let tracks = match res {
                Ok(res) => res,
                Err(err) => {
                    warn!("failed to retrieve songs from Spotify: {:?}", err);
                    return Err(err.into());
                }
            };

            songs.extend(tracks.into_iter().map(|track| self.track_to_song(track)));
        }

        {
            let mut cache_guard = self.cache_song.write().unwrap();
            for song in songs.iter() {
                cache_guard.insert(song.track_id.clone(), song.clone());
            }
        }

        Ok(songs)
    }

    /// return the cover and if it was downloaded
    pub async fn retrieve_cover(
        &self,
        artist: &str,
        title: &str,
    ) -> Result<(Option<String>, bool)> {
        let state = self.state.lock().unwrap().clone();
        let Some((rspotify_client, _librespot_session)) = state.clients() else {
            bail!("no connected to Spotify");
        };

        // strip karaoke release name, won't be needed once https://gitlab.freedesktop.org/gdesmott/karapulse/-/issues/4
        // is fixed.
        lazy_static::lazy_static! {
            static ref STRIP_RELEASE_NAME: regex::Regex = regex::Regex::new("(.+) \\[(.+)").unwrap();
        }

        if !self.settings.spotify().download_external_covers {
            // disabled in config file
            return Ok((None, false));
        }

        let fields = if let Some(capture) = STRIP_RELEASE_NAME.captures(title) {
            let title = capture.get(1).unwrap().as_str();
            format!("{} {}", artist, title)
        } else {
            format!("{} {}", artist, title)
        };

        let cached = {
            let cache_guard = self.cache_cover.read().unwrap();
            // TODO: we could save an allocation on cache-hit using
            // https://stackoverflow.com/questions/45786717/how-to-implement-hashmap-with-two-keys/45795699#45795699
            cache_guard.get(&fields).cloned()
        };

        if let Some(cached) = cached {
            return Ok((cached, false));
        }

        if let Err(not_until) = self.cover_limiter.check() {
            debug!(
                "maximum cover requests limit (earliest: {:?})",
                not_until.earliest_possible()
            );
            anyhow::bail!("maximum cover requests limit");
        }

        let res = rspotify_client
            .search(
                &fields,
                SearchType::Track,
                None,
                None,
                Some(SEARCH_LIMIT),
                None,
            )
            .await;

        let res = match res {
            Ok(res) => res,
            Err(err) => {
                warn!("failed to retrieve cover from Spotify: {:?}", err);
                return Err(err.into());
            }
        };

        match res {
            SearchResult::Tracks(res) => {
                if res.items.is_empty() {
                    return Ok((None, false));
                }

                let track = &res.items[0];
                let cover = full_track_cover(track);

                {
                    let mut cache_guard = self.cache_cover.write().unwrap();
                    cache_guard.insert(fields, cover.clone());
                }

                Ok((cover, true))
            }
            _ => bail!("did not return tracks result"),
        }
    }

    fn track_to_song(&self, mut track: FullTrack) -> Song {
        let artist = track.artists.remove(0);

        let cover = if self.settings.spotify().covers {
            // Spotify covers have been disabled in config file
            full_track_cover(&track)
        } else {
            None
        };

        Song {
            track_id: track.id.unwrap().id().to_string(),
            artist: artist.name,
            title: track.name.clone(),
            length: Some(track.duration.num_seconds() as i32),
            cover,
            explicit: track.explicit,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Song {
    pub track_id: String,
    pub artist: String,
    pub title: String,
    pub length: Option<i32>,
    pub cover: Option<String>,
    pub explicit: bool,
}

fn full_track_cover(track: &FullTrack) -> Option<String> {
    let mut cover = track
        .album
        .images
        .iter()
        .find(|image| image.width == Some(PREFERRED_COVER_WIDTH));

    if cover.is_none() && !track.album.images.is_empty() {
        // use first size
        cover = Some(&track.album.images[0]);
    };

    cover.map(|image| image.url.clone())
}
