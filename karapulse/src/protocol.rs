// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

// Structures returned as JSON reply. Be careful to not change their serialization
// as that would be a protocol break.

use std::{path::Path, str::FromStr};

use chrono::{DateTime, Utc};
use itertools::Itertools;
use serde::Serialize;

use crate::{db, karapulse, queue, settings, spotify};

#[derive(Debug, PartialEq, Eq)]
pub enum SongId {
    Db(i64),
    Spotify(String),
}

impl Serialize for SongId {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        match self {
            SongId::Db(id) => serializer.serialize_str(&format!("db-{}", id)),
            SongId::Spotify(id) => serializer.serialize_str(&format!("spotify-{}", id)),
        }
    }
}

impl FromStr for SongId {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(id) = s.strip_prefix("db-") {
            let id = i64::from_str(id)?;
            Ok(SongId::Db(id))
        } else if let Some(id) = s.strip_prefix("spotify-") {
            Ok(SongId::Spotify(id.to_string()))
        } else {
            Err(anyhow::anyhow!("unsupported prefix"))
        }
    }
}

impl std::fmt::Display for SongId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SongId::Db(id) => write!(f, "db-{}", id),
            SongId::Spotify(track_id) => write!(f, "spotify-{}", track_id),
        }
    }
}

// search
#[derive(Debug, Serialize)]
pub struct Song {
    pub id: SongId,
    pub artist: String,
    pub title: String,
    pub length: Option<i32>,
    #[serde(rename = "type")]
    pub type_: SongType,
    pub cover: Option<String>,
    pub explicit: bool,
}

#[derive(Debug, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum SongType {
    Cdg,
    Video,
    Spotify,
}

impl Song {
    fn new(s: karapulse::Song) -> Option<Self> {
        match s {
            karapulse::Song::Db(s) => Some(Self::from_db(s)),
            karapulse::Song::Path(_) => None,
            karapulse::Song::Spotify(s) => Some(Self::from_spotify(s)),
        }
    }

    pub fn from_db(s: db::Song) -> Self {
        // assume mp3 files are cdg and the rest is a video file
        let type_ = match Path::new(&s.path).extension().map(|ext| ext.to_str()) {
            Some(Some("mp3")) => SongType::Cdg,
            _ => SongType::Video,
        };

        Self {
            id: SongId::Db(s.rowid),
            artist: s.artist,
            title: s.title,
            length: s.length,
            type_,
            cover: None, // FIXME: search for cover
            explicit: false,
        }
    }

    pub fn from_spotify(s: spotify::Song) -> Self {
        Self {
            id: SongId::Spotify(s.track_id),
            artist: s.artist,
            title: s.title,
            length: s.length,
            type_: SongType::Spotify,
            cover: s.cover,
            explicit: s.explicit,
        }
    }

    /// return if the cover has been downloaded
    pub async fn retrieve_cover(&mut self, spotify: &spotify::Spotify) -> bool {
        match &self {
            Song {
                cover: None,
                artist,
                title,
                type_: SongType::Cdg,
                ..
            }
            | Song {
                cover: None,
                artist,
                title,
                type_: SongType::Video,
                ..
            } => {
                if let Ok((Some(cover), downloaded)) = spotify.retrieve_cover(artist, title).await {
                    self.cover = Some(cover);
                    downloaded
                } else {
                    false
                }
            }
            _ => false,
        }
    }
}

#[derive(Debug, Serialize)]
pub struct SearchResponse(Vec<Song>);

impl SearchResponse {
    pub fn new(songs: Vec<Song>) -> Self {
        Self(songs)
    }

    pub async fn retrieve_cover(&mut self, spotify: &spotify::Spotify) {
        let mut futures = vec![];

        for song in self.0.iter_mut() {
            futures.push(song.retrieve_cover(spotify));
        }

        futures::future::join_all(futures).await;
    }
}

// history
#[derive(Debug, Serialize)]
struct History {
    pub id: i64,
    pub user: String,
    pub song: Option<Song>,
    pub queued: DateTime<Utc>,
    pub played: Option<DateTime<Utc>>,
}

impl History {
    async fn from_db_history(s: db::History, spotify: &spotify::Spotify) -> anyhow::Result<Self> {
        match s.song {
            db::HistorySong::Path(_) => Err(anyhow::anyhow!("path songs not sent to clients")),
            db::HistorySong::Db(song) => Ok(Self {
                id: s.rowid,
                user: s.user,
                song: Some(Song::from_db(song)),
                queued: s.queued,
                played: s.played,
            }),
            db::HistorySong::Spotify(track) => {
                let song = spotify.song_from_id(&track).await?;

                Ok(Self {
                    id: s.rowid,
                    user: s.user,
                    song: Some(Song::from_spotify(song)),
                    queued: s.queued,
                    played: s.played,
                })
            }
        }
    }
}

#[derive(Debug, Serialize)]
pub struct HistoryResponse(Vec<History>);

impl HistoryResponse {
    pub async fn new(history: Vec<db::History>, spotify: &spotify::Spotify) -> Self {
        // spotify songs can be all retrieved using a single query
        let (history_spotify, history_not_spotify): (Vec<_>, Vec<_>) =
            history.into_iter().partition(|h| match h.song {
                db::HistorySong::Db(_) => false,
                db::HistorySong::Path(_) => false,
                db::HistorySong::Spotify(_) => true,
            });

        let mut result = vec![];

        for h in history_not_spotify.into_iter() {
            if let Ok(h) = History::from_db_history(h, spotify).await {
                result.push(h);
            }
        }

        let spotify_ids = history_spotify.iter().map(|h| match &h.song {
            db::HistorySong::Db(_) | db::HistorySong::Path(_) => unreachable!(),
            db::HistorySong::Spotify(id) => id.clone(),
        });

        match spotify.songs_from_id(spotify_ids.collect()).await {
            Ok(spotify_songs) => {
                let spotify_songs = history_spotify
                    .into_iter()
                    .zip(spotify_songs.into_iter())
                    .map(|(h, s)| History {
                        id: h.rowid,
                        user: h.user,
                        song: Some(Song::from_spotify(s)),
                        queued: h.queued,
                        played: h.played,
                    });

                result.extend(spotify_songs);
            }
            Err(err) => {
                warn!("failed to retrieve spotify songs from history: {err}");
            }
        }

        // resort history
        let result = result
            .into_iter()
            .sorted_by(|a, b| b.played.cmp(&a.played))
            .collect();

        Self(result)
    }

    pub async fn retrieve_cover(&mut self, spotify: &spotify::Spotify) {
        let mut futures = vec![];

        for h in self.0.iter_mut() {
            if let Some(song) = h.song.as_mut() {
                futures.push(song.retrieve_cover(spotify));
            }
        }

        futures::future::join_all(futures).await;
    }
}

// status
#[derive(Debug, Serialize)]
pub struct StatusResponseSong {
    pub user: String,
    pub song: Song,
    #[serde(skip_serializing_if = "Option::is_none")]
    eta: Option<u32>,
}

impl StatusResponseSong {
    pub fn new(item: queue::Item, eta: Option<u32>) -> Option<Self> {
        match Song::new(item.song) {
            Some(song) => Some(Self {
                user: item.user,
                song,
                eta,
            }),
            None => None,
        }
    }
}

#[derive(Debug, Serialize)]
pub struct StatusResponse {
    pub state: StateResponse,
    pub current_song: Option<StatusResponseSong>,
    pub position: Option<u64>,
    pub queue: Vec<StatusResponseSong>,
}

#[derive(Debug, Serialize)]
pub enum StateResponse {
    Waiting,
    Playing,
    Paused,
}

impl StatusResponse {
    pub fn new(
        state: karapulse::State,
        current_song: Option<StatusResponseSong>,
        position: Option<u64>,
        queue: Vec<StatusResponseSong>,
    ) -> Self {
        let state = match state {
            karapulse::State::Waiting => StateResponse::Waiting,
            karapulse::State::Playing | karapulse::State::Announcing => StateResponse::Playing,
            karapulse::State::Paused(_) => StateResponse::Paused,
        };

        Self {
            state,
            current_song,
            position,
            queue,
        }
    }

    pub async fn retrieve_cover(&mut self, spotify: &spotify::Spotify) {
        if let Some(current) = self.current_song.as_mut() {
            current.song.retrieve_cover(spotify).await;
        }

        for song in self.queue.iter_mut() {
            song.song.retrieve_cover(spotify).await;
        }
    }
}

#[derive(Debug, serde::Deserialize)]
pub struct AdminAuthRequest {
    pub password: String,
}

#[derive(Debug, serde::Serialize)]
pub struct AdminAuthReply {
    pub token: String,
}

#[derive(Debug, serde::Serialize)]
pub struct ConfigReply {
    pub config: settings::Inner,
    // only set if the password has been updated and so the old token invalidated
    #[serde(skip_serializing_if = "Option::is_none")]
    pub token: Option<String>,
}
