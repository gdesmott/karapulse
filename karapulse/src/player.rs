// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use crate::common;
use crate::karapulse::{Message, Song};
use anyhow::{anyhow, Result};
use futures::prelude::*;
use get_if_addrs::get_if_addrs;
use gst::prelude::*;
use std::cell::RefCell;
use std::path::{Path, PathBuf};
use std::sync::{Arc, Mutex};
use tokio::sync::mpsc::Sender;
use url::Url;

use gtk::{gdk, prelude::*};

pub struct Player {
    playbin: gst::Element,
    tx: Sender<Message>,
    announce_handle: Arc<Mutex<RefCell<Option<tokio::task::JoinHandle<()>>>>>,
    _bus_handle: tokio::task::JoinHandle<()>,

    pub widget: gstgtk4::RenderWidget,

    overlay_ip: gst::Element,
    overlay_qr: Option<gst::Element>,
    overlay_message: gst::Element,
    overlay_song: gst::Element,

    tunnel_url: Mutex<Option<String>>,
}

impl Player {
    pub fn new(tx: Sender<Message>) -> Result<Player> {
        let picture = gtk::Picture::new();

        let gtksink = gst::ElementFactory::make("gtk4paintablesink")
            .build()
            .unwrap();

        let paintable = gtksink.property::<gdk::Paintable>("paintable");
        let widget = gstgtk4::RenderWidget::new(&gtksink);
        picture.set_parent(&widget);

        let video_sink = if paintable
            .property::<Option<gdk::GLContext>>("gl-context")
            .is_some()
        {
            gst::ElementFactory::make("glsinkbin")
                .property("sink", &gtksink)
                .build()
                .unwrap()
        } else {
            gtksink
        };

        let playbin = gst::ElementFactory::make("playbin3")
            .build()
            .map_err(|_| anyhow!("Missing element playbin3"))?;

        playbin.set_property("video-sink", &video_sink);

        let spotify_cache = crate::spotify::cache_dir()?;

        playbin.connect("source-setup", false, move |args| {
            let source = args[1].get::<gst::Element>().unwrap();
            if source.type_().name() == "KarapulseSpotifySrcBin" {
                // configure spotify source

                // rely on the spotify.rs to have cached the credentials
                let cache = spotify_cache.to_str().unwrap();
                source.set_property("cache-credentials", cache);
                source.set_property("cache-files", cache);
            }

            None
        });

        playbin.connect("element-setup", false, |args| {
            let element = args[1].get::<gst::Element>().unwrap();
            // improve rendering of Spotify lyrics
            if element.type_().name() == "GstTextOverlay"
                && element.name() != "message"
                && element.name() != "ip"
                && element.name() != "song"
            {
                element.set_property("draw-outline", false);
                element.set_property("draw-shadow", false);

                element.set_property_from_str("valignment", "top");
                element.set_property("ypad", 150i32);

                // subtitleoverlay will change this back
                element.connect_notify(Some("valignment"), |element, _pspec| {
                    let v = element.property_value("valignment");
                    let (_, v) = gst::glib::EnumValue::from_value(&v).unwrap();
                    if v.nick() != "top" {
                        element.set_property_from_str("valignment", "top");
                    }
                });
            }

            None
        });

        picture.set_paintable(Some(&paintable));

        // video overlays
        let video_filter = gst::Bin::new();

        let overlay_ip = gst::ElementFactory::make("textoverlay")
            .name("ip")
            .property("shaded-background", true)
            .property_from_str("valignment", "bottom")
            .property_from_str("halignment", "center")
            .build()?;

        let overlay_qr = gst::ElementFactory::make("qroverlay")
            .property("x", 1f32)
            .property("y", 99f32)
            .property_from_str("qrcode-error-correction", "Level H")
            .property("size", 20f32)
            .property("case-sensitive", true)
            .build()
            .map_err(|err| warn!("QR overlay disabled: {err}"))
            .ok();

        let overlay_song = gst::ElementFactory::make("textoverlay")
            .name("song")
            .property("shaded-background", true)
            .property_from_str("valignment", "top")
            .property_from_str("halignment", "left")
            .build()?;

        let overlay_message = gst::ElementFactory::make("textoverlay")
            .name("message")
            .property("shaded-background", true)
            .property_from_str("valignment", "center")
            .property_from_str("halignment", "center")
            .build()?;

        video_filter.add_many([&overlay_ip, &overlay_song, &overlay_message])?;

        if let Some(overlay_qr) = overlay_qr.as_ref() {
            video_filter.add(overlay_qr)?;

            overlay_ip.link(overlay_qr)?;
            overlay_qr.link(&overlay_song)?;
        } else {
            overlay_ip.link(&overlay_song)?;
        }

        overlay_song.link(&overlay_message)?;

        let sink = overlay_ip.static_pad("video_sink").unwrap();
        let sink = gst::GhostPad::builder_with_target(&sink)?.build();
        video_filter.add_pad(&sink)?;
        sink.set_active(true)?;

        let src = overlay_message.static_pad("src").unwrap();
        let src = gst::GhostPad::builder_with_target(&src)?.build();
        video_filter.add_pad(&src)?;
        src.set_active(true)?;

        playbin.set_property("video-filter", &video_filter);

        let bus = playbin.bus().unwrap();
        let tx_clone = tx.clone();
        let bus_handle = common::runtime().spawn(async move {
            let mut messages = bus.stream();
            let mut last_failed_uri = None;

            while let Some(msg) = messages.next().await {
                match msg.view() {
                    gst::MessageView::Eos(..) => {
                        debug!("Reached EOS");
                        tx_clone.send(Message::PlayingDone).await.unwrap();
                    }
                    gst::MessageView::Error(err) => {
                        error!(
                            "Error from {:?}: {} ({:?})",
                            err.src().map(|s| s.path_string()),
                            err.error(),
                            err.debug()
                        );

                        // check if this URI has already failed so we don't skip songs multiple times
                        let source_uri = err
                            .src()
                            .and_then(get_pipeline)
                            .map(|pipeline| pipeline.property::<String>("uri"));

                        if source_uri == last_failed_uri {
                            continue;
                        }

                        if source_uri.is_some() {
                            last_failed_uri = source_uri;
                        }

                        tx_clone.send(Message::PlayingDone).await.unwrap();
                    }
                    _ => (),
                }
            }
        });

        let player = Player {
            playbin,
            tx,
            announce_handle: Arc::new(Mutex::new(RefCell::new(None))),
            _bus_handle: bus_handle,
            widget,
            overlay_ip,
            overlay_message,
            overlay_qr,
            overlay_song,
            tunnel_url: Mutex::new(None),
        };

        player.update_ip();

        Ok(player)
    }

    /// The HTTP urls clients can use to connect to the server
    fn web_urls(&self) -> Vec<String> {
        let tunnel_url = self.tunnel_url.lock().unwrap();

        match tunnel_url.as_ref() {
            Some(url) => vec![url.clone()],
            None => {
                // Ignore loopback and virtual interfaces
                // FIXME: don't hardcode port
                get_if_addrs()
                    .unwrap()
                    .iter()
                    .filter(|iface| !iface.name.starts_with("virbr"))
                    .filter(|iface| !iface.name.starts_with("vmnet"))
                    .filter(|iface| !iface.name.starts_with("wg"))
                    .filter(|iface| !iface.name.starts_with("docker"))
                    .filter(|iface| !iface.name.starts_with("br-"))
                    .map(get_if_addrs::Interface::ip)
                    .filter(|ip| !ip.is_loopback())
                    .filter(|ip| ip.is_ipv4())
                    .map(|ip| format!("http://{}:1848", ip))
                    .collect::<Vec<String>>()
            }
        }
    }

    /// update text and QR code overlay with the current IP
    fn update_ip(&self) {
        let urls = self.web_urls();

        let text = self.web_urls().join("\n");
        self.overlay_ip.set_property("text", text);

        if let (Some(url), Some(overlay_qr)) = (urls.first(), self.overlay_qr.as_ref()) {
            overlay_qr.set_property("data", url);
        }
    }

    fn start_pipeline(&self, source: Source) -> Result<()> {
        self.playbin.set_state(gst::State::Null)?;

        match source {
            Source::Path(path) => self.set_source_path(&path)?,
            Source::Spotify(track_id) => self.set_source_spotify(&track_id)?,
            Source::Background => self.set_source_background()?,
        }

        self.play()
    }

    fn set_source_path(&self, media: &Path) -> Result<()> {
        let path = media
            .canonicalize()
            .map_err(|_| anyhow::anyhow!("Cannot find media: {}", media.display()))?;
        let uri = Url::from_file_path(path.to_str().unwrap()).unwrap();
        self.playbin.set_property("uri", uri.to_string());

        let mut suburi: Option<String> = None;
        // Assume mp3 file have a CDG associated
        if let Some(ext) = media.extension() {
            if ext == "mp3" {
                let path = path.with_extension("cdg");
                let url = Url::from_file_path(path.to_str().unwrap()).unwrap();
                suburi = Some(url.to_string());
            }
        }
        self.playbin.set_property("suburi", suburi);

        Ok(())
    }

    fn set_source_spotify(&self, track_id: &str) -> Result<()> {
        let track_id = format!("spotify:track:{}", track_id);

        self.playbin.set_property("uri", track_id);
        Ok(())
    }

    fn set_source_background(&self) -> Result<()> {
        // use a video format supported by gtk4paintablesink so we can use the video overlay composition meta
        self.playbin.set_property("uri", "testbin://video,pattern=black,caps=[video/x-raw,width=1920,height=1080,framerate=30/1,format=BGRA]");
        self.playbin.set_property("suburi", None::<String>);
        Ok(())
    }

    pub fn display_message(&self, message: &str, timer: Option<u64>) -> Result<()> {
        let txt = gst::glib::markup_escape_text(message);

        self.overlay_message.set_property("text", txt);
        self.overlay_song.set_property("text", None::<String>);

        self.remove_announce_handle();
        self.start_pipeline(Source::Background)?;

        if let Some(timer) = timer {
            // setup timer to notify caller when the message is done being displayed
            let playbin_weak = self.playbin.downgrade();
            let tx = self.tx.clone();

            let handle = common::runtime().spawn(async move {
                let mut interval = tokio::time::interval(std::time::Duration::from_secs(1));

                loop {
                    interval.tick().await;

                    let playbin = match playbin_weak.upgrade() {
                        Some(playbin) => playbin,
                        None => {
                            break;
                        }
                    };

                    match playbin.query_position::<gst::ClockTime>() {
                        Some(pos) if pos.seconds() > timer => {
                            trace!("display message done");
                            tx.send(Message::DisplayMessageDone).await.unwrap();
                            break;
                        }
                        _ => {}
                    }
                }
            });

            let mut announce_handle = self.announce_handle.lock().unwrap();
            announce_handle.get_mut().replace(handle);
        }

        Ok(())
    }

    pub fn open_song(&self, song: &Song) -> Result<()> {
        debug!("opening {:?}", song);

        self.overlay_message.set_property("text", None::<String>);

        let song_info = song
            .info()
            .map(|info| format!("{} - {}", info.artist, info.title));
        self.overlay_song.set_property("text", song_info);

        self.start_pipeline(Source::from(song))
    }

    fn set_state(&self, state: gst::State) -> Result<()> {
        debug!("set pipeline to {:?}", state);
        self.playbin.set_state(state)?;
        Ok(())
    }

    pub fn pause(&self) -> Result<()> {
        self.set_state(gst::State::Paused)
    }

    pub fn play(&self) -> Result<()> {
        self.set_state(gst::State::Playing)
    }

    pub fn stop(&self) -> Result<()> {
        debug!("Display background");

        self.display_message(crate::common::gettext("Waiting for songs").as_str(), None)
    }

    pub fn get_position(&self) -> Option<u64> {
        self.playbin
            .query_position::<gst::ClockTime>()
            .map(|pos| pos.seconds())
    }

    fn remove_announce_handle(&self) {
        let announce_handle = self.announce_handle.lock().unwrap();

        if let Some(source_id) = announce_handle.take() {
            source_id.abort();
        }
    }

    pub fn restart_song(&self) -> Result<()> {
        // seeking is not working with cdg/mp3 so restart the whole pipeline #yolo
        self.set_state(gst::State::Null)?;
        self.play()?;

        Ok(())
    }

    pub fn tunnel_setup(&self, tunnel_url: String) -> Result<()> {
        {
            let mut t = self.tunnel_url.lock().unwrap();
            t.replace(tunnel_url);
        }
        self.update_ip();
        Ok(())
    }
}

impl Drop for Player {
    fn drop(&mut self) {
        self.remove_announce_handle();
        self.playbin.set_state(gst::State::Null).unwrap();
    }
}

enum Source {
    Path(PathBuf),
    Spotify(String),
    Background,
}

impl From<&Song> for Source {
    fn from(song: &Song) -> Self {
        match song {
            Song::Path(path) => Source::Path(path.clone()),
            Song::Db(song) => Source::Path(song.path()),
            Song::Spotify(song) => Source::Spotify(song.track_id.clone()),
        }
    }
}

fn get_pipeline<O: IsA<gst::Object>>(obj: &O) -> Option<gst::Pipeline> {
    let mut parent = obj.as_ref().clone();
    while let Some(new_parent) = parent.parent() {
        parent = new_parent;
    }

    parent.downcast().ok()
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::common::init;
    use crate::tests::TestMedia;
    use std::{
        sync::atomic::{AtomicBool, Ordering},
        thread, time,
    };

    struct Test {
        player: Player,
        window: gtk::Window,
        done: Arc<AtomicBool>,
    }

    impl Test {
        fn new() -> Test {
            let (tx, mut rx) = tokio::sync::mpsc::channel(100);

            let done = Arc::new(AtomicBool::new(false));
            let done_clone = done.clone();

            gst::glib::spawn_future_local(async move {
                while let Some(msg) = rx.recv().await {
                    if let Message::PlayingDone = msg {
                        done_clone.store(true, Ordering::Relaxed);
                    }
                }
            });

            let window = gtk::Window::new();
            let player = Player::new(tx).unwrap();

            window.set_child(Some(&player.widget));

            let test = Test {
                player,
                window: window.clone(),
                done,
            };

            window.set_size_request(800, 600);
            window.set_visible(true);

            test
        }

        fn open_media(&mut self, media: TestMedia) {
            self.player.open_song(&Song::Path(media.path())).unwrap();
        }

        fn run_until_eos(&self) {
            let context = gst::glib::MainContext::ref_thread_default();
            while !self.done.load(Ordering::Relaxed) {
                context.iteration(true);
            }
        }
    }

    impl Drop for Test {
        fn drop(&mut self) {
            self.window.destroy();
        }
    }

    #[test]
    #[ignore]
    fn player_tests() {
        let _ = env_logger::try_init();
        init().unwrap();

        let context = gst::glib::MainContext::ref_thread_default();
        let _guard = context.acquire().unwrap();

        play_twice();
        play_next();
        pause();
        stop();
        play_cdg_alone();
        play_video_then_cdg();
        play_cdg_then_video();
        restart_song();
    }

    fn play_twice() {
        println!("play_twice");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        assert_eq!(test.player.get_position(), None);
        test.run_until_eos();
        assert_eq!(test.player.get_position(), Some(2));

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();
    }

    fn play_next() {
        println!("play_next");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        thread::sleep(time::Duration::from_millis(500));

        test.open_media(TestMedia::Video2_2s);
        test.run_until_eos();
    }

    fn pause() {
        println!("pause");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        thread::sleep(time::Duration::from_millis(500));

        test.player.pause().unwrap();
        thread::sleep(time::Duration::from_millis(500));
        test.player.play().unwrap();
        test.run_until_eos();
    }

    fn stop() {
        println!("stop");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        test.player.stop().unwrap();
    }

    fn play_cdg_alone() {
        println!("play_cdg_alone");
        let mut test = Test::new();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();
    }

    fn play_video_then_cdg() {
        println!("play_video_then_cdg");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();
    }

    fn play_cdg_then_video() {
        println!("play_cdg_then_video");
        let mut test = Test::new();

        test.open_media(TestMedia::Audio);
        test.run_until_eos();

        test.open_media(TestMedia::Video2s);
        test.run_until_eos();
    }

    fn restart_song() {
        println!("restart_song");
        let mut test = Test::new();

        test.open_media(TestMedia::Video2s);
        thread::sleep(time::Duration::from_millis(500));

        test.player.restart_song().unwrap();
        thread::sleep(time::Duration::from_millis(500));
        test.player.play().unwrap();
        test.run_until_eos();
    }
}
