// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use anyhow::{anyhow, Result};
use gtk::prelude::*;
use serde::Serialize;
use std::cell::{Cell, RefCell};
use std::ops;
use std::path::PathBuf;
use std::rc::Rc;

use gtk::gdk;
use tokio::sync::{
    mpsc::{Receiver, Sender},
    oneshot,
};

use crate::{db, player, protocol, queue, spotify};

// The fallback song duration, in seconds, used to compute queue ETA
const FALLBACK_ETA_DURATION: u32 = 3 * 60;

// How long, in seconds, the announce message is displayed between two songs
const ANNOUNCE_DURATION: u64 = 5;

#[allow(dead_code)]
#[derive(Debug, PartialEq, Eq, Clone, Copy, Serialize)]
pub enum State {
    Waiting,
    Announcing,
    Playing,
    Paused(PausedState), // the state we paused from
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Serialize)]
pub enum PausedState {
    Waiting,
    Announcing,
    Playing,
}

#[derive(Debug)]
pub enum Message {
    Play,
    Pause,
    Next,
    PlayingDone,
    Enqueue {
        user: String,
        song: Song,
    },
    GetStatus {
        reply_tx: Option<oneshot::Sender<Reply>>,
    },
    DisplayMessageDone,
    RestartSong,
    RemoveSong {
        song_id: protocol::SongId,
    },
    TunnelSetup {
        tunnel_url: String,
    },
}

impl PartialEq for Message {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Message::Play, Message::Play) => true,
            (Message::Pause, Message::Pause) => true,
            (Message::Next, Message::Next) => true,
            (Message::PlayingDone, Message::PlayingDone) => true,
            // reply_tx don't have to be equals
            (Message::GetStatus { .. }, Message::GetStatus { .. }) => true,
            (
                Message::Enqueue {
                    user: user_a,
                    song: song_a,
                },
                Message::Enqueue {
                    user: user_b,
                    song: song_b,
                },
            ) => user_a == user_b && song_a == song_b,
            (Message::RestartSong, Message::RestartSong) => true,
            (
                Message::RemoveSong { song_id: song_id_a },
                Message::RemoveSong { song_id: song_id_b },
            ) => song_id_a == song_id_b,
            _ => false,
        }
    }
}

#[derive(Debug)]
pub enum Reply {
    Status { status: protocol::StatusResponse },
}

pub struct KarapulseInner {
    player: player::Player,
    window: gtk::Window,
    queue: RefCell<queue::Queue>,
    state: Cell<State>,
    current_song: RefCell<Option<queue::Item>>,
    db: db::DB,
}

pub struct Karapulse {
    inner: Rc<KarapulseInner>,
}

impl KarapulseInner {
    pub fn new(player: player::Player, db: db::DB, window: gtk::Window) -> Self {
        Self {
            player,
            window,
            queue: RefCell::new(queue::Queue::new()),
            state: Cell::new(State::Paused(PausedState::Waiting)),
            current_song: RefCell::new(None),
            db,
        }
    }

    pub fn enqueue(&self, user: &str, song: Song, history_id: Option<i64>) -> Result<()> {
        let history_id = if let Some(id) = history_id {
            id
        } else {
            match &song {
                Song::Db(song) => self.db.add_history_db(song, user)?,
                Song::Path(p) => self
                    .db
                    .add_history_external(db::ExternalHistory::Path(p.clone()), user)?,
                Song::Spotify(song) => self.db.add_history_external(
                    db::ExternalHistory::Spotify(song.track_id.clone()),
                    user,
                )?,
            }
        };

        let res = self.queue.borrow_mut().add(user, song, Some(history_id));

        match res {
            Ok(_) => {
                if self.state() == State::Waiting {
                    self.play_next()?;
                }

                Ok(())
            }
            Err(queue::AddError::AlreadyThere) => {
                debug!("song already queued, ignore it");
                Ok(())
            }
        }
    }

    fn set_state(&self, state: State) {
        let current_state = self.state.get();
        if current_state != state {
            debug!("set state: {:?} -> {:?}", current_state, state,);
            self.state.set(state);
        }
    }

    fn play_next(&self) -> Result<()> {
        let item = { self.queue.borrow_mut().next_item() };

        match item {
            Some(item) => {
                if let Some(history_id) = item.history_id {
                    self.db.set_history_played(history_id)?;
                }

                debug!("next: {:?}", item.song);

                self.current_song.replace(Some(item));
                self.announce_current_song()?;
            }
            None => {
                if self.state.get() != State::Waiting {
                    self.player.stop()?;
                    debug!("waiting for songs");
                    self.set_state(State::Waiting);
                    self.current_song.replace(None);
                }
            }
        }

        Ok(())
    }

    pub fn play(&self) -> Result<()> {
        match self.state.get() {
            State::Paused(PausedState::Waiting) => self.play_next()?,
            State::Paused(PausedState::Playing) => {
                self.set_state(State::Playing);
                self.player.play()?;
            }
            State::Paused(PausedState::Announcing) => {
                self.set_state(State::Announcing);
                self.player.play()?;
            }
            State::Waiting => self.play_next()?,
            State::Playing | State::Announcing => {}
        }

        Ok(())
    }

    fn pause(&self) -> Result<()> {
        match self.state() {
            State::Playing => {
                self.player.pause()?;
                self.set_state(State::Paused(PausedState::Playing));
            }
            State::Announcing => {
                self.player.pause()?;
                self.set_state(State::Paused(PausedState::Announcing));
            }
            State::Waiting => self.set_state(State::Paused(PausedState::Waiting)),
            State::Paused(_) => {}
        }

        Ok(())
    }

    fn toggle_pause(&self) -> Result<()> {
        match self.state() {
            State::Playing | State::Announcing => self.pause(),
            State::Paused(_) => self.play(),
            State::Waiting => Ok(()),
        }
    }

    pub fn state(&self) -> State {
        self.state.get()
    }

    fn announce_current_song(&self) -> Result<()> {
        let item = self.current_song.borrow().clone().unwrap();

        if let Some(info) = item.song.info() {
            let text = format!("🎤 {} 🎤\n\n{} - {}", item.user, info.artist, info.title);
            self.player
                .display_message(&text, Some(ANNOUNCE_DURATION))?;

            self.set_state(State::Announcing);
        } else {
            debug!("no song info, play song right away");
            self.play_current_song()?;
        }

        Ok(())
    }

    fn play_current_song(&self) -> Result<()> {
        let item = self.current_song.borrow().clone().unwrap();

        if self.player.open_song(&item.song).is_ok() {
            self.set_state(State::Playing);
        } else {
            self.current_song.replace(None);
            self.play_next()?;
        }

        Ok(())
    }

    pub fn get_status(&self, reply_tx: oneshot::Sender<Reply>) -> Result<()> {
        let state = self.state();
        let current_song = self.current_song.borrow().clone();
        let position = match state {
            State::Playing => self.player.get_position(),
            _ => None,
        };
        let queue = self.queue.borrow().snapshot();

        // Compute ETA for each song in the queue
        let duration = current_song.as_ref().map(|i| i.song.duration());

        let mut eta = match (position, duration) {
            (Some(position), Some(duration)) => duration - (position as u32),
            // Assume song is already half played
            _ => FALLBACK_ETA_DURATION / 2,
        };

        let mut queue_response = Vec::new();
        for i in queue {
            let increment = i.song.duration();
            if let Some(song) = protocol::StatusResponseSong::new(i, Some(eta)) {
                queue_response.push(song);
            }
            eta += increment;
        }

        let current_song = current_song.and_then(|s| protocol::StatusResponseSong::new(s, None));

        let status = protocol::StatusResponse::new(state, current_song, position, queue_response);
        reply_tx
            .send(Reply::Status { status })
            .map_err(|e| anyhow!("Failed to send status reply: {:?}", e))?;
        Ok(())
    }

    fn restart_song(&self) -> Result<()> {
        match self.state() {
            State::Playing | State::Paused(_) => {
                self.player.restart_song()?;
            }
            State::Announcing | State::Waiting => {}
        }

        Ok(())
    }

    fn remove_song(&self, song_id: protocol::SongId) -> Result<()> {
        if let Some(queue::Item {
            history_id: Some(history_id),
            ..
        }) = self.queue.borrow_mut().remove_song_with_id(song_id)
        {
            self.db.delete_history(history_id)?;
        }

        Ok(())
    }

    /// called when HTTP tunnel has been setup
    fn tunnel_setup(&self, tunnel_url: String) -> Result<()> {
        self.player.tunnel_setup(tunnel_url)
    }
}

impl ops::Deref for Karapulse {
    type Target = KarapulseInner;

    fn deref(&self) -> &KarapulseInner {
        &self.inner
    }
}

impl Karapulse {
    pub fn new(
        window: gtk::Window,
        tx: Sender<Message>,
        mut rx: Receiver<Message>,
        db: db::DB,
    ) -> Self {
        let player = player::Player::new(tx).unwrap();
        window.set_child(Some(&player.widget));

        let inner = KarapulseInner::new(player, db, window.clone());
        let inner = Rc::new(inner);

        let inner_weak = Rc::downgrade(&inner);

        // TODO: would be nice to use runtime().spawn() instead to reduce the load on the main thread
        gst::glib::spawn_future_local(async move {
            while let Some(msg) = rx.recv().await {
                let karapulse = match inner_weak.upgrade() {
                    Some(inner) => inner,
                    None => break,
                };

                let res = match msg {
                    Message::Play => karapulse
                        .play()
                        .map_err(|e| anyhow::anyhow!("Play failed: {}", e)),
                    Message::Pause => karapulse
                        .pause()
                        .map_err(|e| anyhow::anyhow!("Pause failed: {}", e)),
                    Message::Next => karapulse
                        .play_next()
                        .map_err(|e| anyhow::anyhow!("Next failed: {}", e)),
                    Message::PlayingDone => karapulse
                        .play_next()
                        .map_err(|e| anyhow::anyhow!("PlayingDone failed: {}", e)),
                    Message::Enqueue { user, song } => karapulse
                        .enqueue(&user, song, None)
                        .map_err(|e| anyhow::anyhow!("Enqueue failed: {}", e)),
                    Message::GetStatus { mut reply_tx } => {
                        let reply_tx = reply_tx.take().unwrap();
                        karapulse
                            .get_status(reply_tx)
                            .map_err(|e| anyhow::anyhow!("GetStatus failed: {}", e))
                    }
                    Message::DisplayMessageDone => karapulse
                        .play_current_song()
                        .map_err(|e| anyhow::anyhow!("DisplayMessageDone failed: {}", e)),
                    Message::RestartSong => karapulse
                        .restart_song()
                        .map_err(|e| anyhow::anyhow!("RestartSong failed: {}", e)),
                    Message::RemoveSong { song_id } => karapulse
                        .remove_song(song_id)
                        .map_err(|e| anyhow::anyhow!("Remove song failed: {}", e)),
                    Message::TunnelSetup { tunnel_url } => karapulse
                        .tunnel_setup(tunnel_url)
                        .map_err(|e| anyhow::anyhow!("Tunnel setup failed: {}", e)),
                };

                if let Err(e) = res {
                    error!("{}", e.to_string())
                }
            }
        });

        let controller = gtk::EventControllerKey::new();

        // Hide cursor in fullscreen
        window.connect_fullscreened_notify(|window| {
            let cursor = if window.is_fullscreen() {
                gdk::Cursor::from_name("none", None).unwrap()
            } else {
                gdk::Cursor::from_name("default", None).unwrap()
            };

            window.set_cursor(Some(&cursor));
        });

        let inner_weak = Rc::downgrade(&inner);
        controller.connect_key_pressed(move |_controller, key, _key_code, _state| {
            let karapulse = match inner_weak.upgrade() {
                Some(inner) => inner,
                None => return gst::glib::Propagation::Stop,
            };
            let window = &karapulse.window;

            match key {
                gdk::Key::f => {
                    if window.is_fullscreen() {
                        window.unfullscreen();
                    } else {
                        window.fullscreen();
                    }
                }
                gdk::Key::Escape => {
                    window.unfullscreen();
                }
                gdk::Key::space => karapulse
                    .toggle_pause()
                    .unwrap_or_else(|e| error!("failed to toggle pause: {}", e.to_string())),
                _ => {}
            }

            gst::glib::Propagation::Stop
        });

        window.add_controller(controller);

        window.set_size_request(800, 600);
        window.fullscreen();
        window.set_visible(true);
        window.present();

        Self { inner }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Song {
    Path(PathBuf),
    Db(db::Song),
    Spotify(spotify::Song),
}

impl Song {
    pub fn info(&self) -> Option<SongInfo> {
        match self {
            Song::Path(_) => None,
            Song::Db(s) => Some(SongInfo {
                artist: s.artist.clone(),
                title: s.title.clone(),
            }),
            Song::Spotify(s) => Some(SongInfo {
                artist: s.artist.clone(),
                title: s.title.clone(),
            }),
        }
    }

    #[cfg(test)]
    pub fn path(&self) -> PathBuf {
        match self {
            Song::Path(p) => p.clone(),
            Song::Db(song) => song.path(),
            Song::Spotify(_) => panic!("spotify songs have no path"),
        }
    }

    pub fn duration(&self) -> u32 {
        let duration = match self {
            Song::Path(_) => None,
            Song::Db(song) => song.length.map(|l| l as u32),
            Song::Spotify(song) => song.length.map(|l| l as u32),
        };

        duration.unwrap_or(FALLBACK_ETA_DURATION)
    }

    pub fn id(&self) -> protocol::SongId {
        match self {
            Song::Path(_) => unreachable!(),
            Song::Db(song) => protocol::SongId::Db(song.rowid),
            Song::Spotify(song) => protocol::SongId::Spotify(song.track_id.clone()),
        }
    }

    pub async fn from_history_song(
        song: db::HistorySong,
        spotify: &spotify::Spotify,
    ) -> anyhow::Result<Self> {
        match song {
            db::HistorySong::Db(song) => Ok(Song::Db(song)),
            db::HistorySong::Path(p) => Ok(Song::Path(p)),
            db::HistorySong::Spotify(track) => {
                let song = spotify.song_from_id(&track).await?;
                Ok(Song::Spotify(song))
            }
        }
    }
}

#[derive(Debug)]
pub struct SongInfo {
    pub artist: String,
    pub title: String,
}

#[cfg(test)]
mod tests {
    use core::time;
    use std::thread;

    use super::*;
    use crate::common::init;
    use crate::tests::TestMedia;

    struct Test {
        karapulse: Karapulse,
        tx: Sender<Message>,
        window: gtk::Window,
    }

    impl Test {
        fn new() -> Test {
            let (tx, rx) = tokio::sync::mpsc::channel(100);
            let window = gtk::Window::new();
            let db = db::DB::new_memory().unwrap();

            let karapulse = Karapulse::new(window.clone(), tx.clone(), rx, db);

            Test {
                karapulse,
                tx,
                window,
            }
        }

        fn tick(&mut self) {
            let context = gst::glib::MainContext::ref_thread_default();
            while context.pending() {
                context.iteration(false);
            }
        }

        async fn send_msg(&mut self, msg: Message) {
            self.tx.send(msg).await.unwrap();
            self.tick();
        }

        async fn pause(&mut self) {
            self.send_msg(Message::Pause).await;
        }

        async fn play(&mut self) {
            self.send_msg(Message::Play).await;
        }

        async fn next(&mut self) {
            self.send_msg(Message::Next).await;
        }

        fn wait_while(&mut self, state: State) {
            while self.karapulse.state() == state {
                self.tick();
            }
        }

        fn enqueue(&mut self, media: TestMedia) {
            self.karapulse
                .enqueue("test", Song::Path(media.path()), None)
                .unwrap();
        }

        fn enqueue_song(&mut self, media: TestMedia, artist: &str, title: &str) {
            let song = db::Song {
                rowid: 1,
                path: media.path().to_str().unwrap().to_string(),
                artist: artist.to_string(),
                title: title.to_string(),
                length: None,
            };

            self.karapulse
                .enqueue("test", Song::Db(song), None)
                .unwrap();
        }
    }

    impl Drop for Test {
        fn drop(&mut self) {
            self.window.destroy();
        }
    }

    #[tokio::test]
    #[ignore]
    async fn karapulse_tests() {
        let _ = env_logger::try_init();
        init().unwrap();

        let context = gst::glib::MainContext::ref_thread_default();
        let _guard = context.acquire().unwrap();

        play_pause().await;
        play_no_song();
        invalid_media();
        next().await;
        announce().await;
        announce_pause().await;
        announce_next().await;
    }

    async fn play_pause() {
        println!("play_pause");
        let mut test = Test::new();
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));

        // One song in the initial queue
        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Playing);

        // Pause
        test.pause().await;
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Playing));

        // Resume
        test.play().await;
        assert_eq!(test.karapulse.state(), State::Playing);

        // Wait for the song to finish
        test.wait_while(State::Playing);
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.pause().await;
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));
        test.play().await;
        assert_eq!(test.karapulse.state(), State::Waiting);
    }

    fn play_no_song() {
        println!("play_no_song");
        let mut test = Test::new();

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
    }

    fn invalid_media() {
        println!("invalid_media");
        /* Play, enqueue invalid */
        let mut test = Test::new();

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Invalid);
        assert_eq!(test.karapulse.state(), State::Waiting);

        /* Enqueue invalid, play */
        let mut test = Test::new();
        test.enqueue(TestMedia::Invalid);
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Waiting);

        /* Enqueue invalid, enqueue valid, play */
        let mut test = Test::new();
        test.enqueue(TestMedia::Invalid);
        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Playing);

        /* Enqueue valid, enqueue invalid, play */
        let mut test = Test::new();
        test.enqueue(TestMedia::Video2s);
        test.enqueue(TestMedia::Invalid);
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));

        test.karapulse.play().unwrap();
        assert_eq!(test.karapulse.state(), State::Playing);

        test.wait_while(State::Playing);
        assert_eq!(test.karapulse.state(), State::Waiting);
    }

    async fn next() {
        println!("next");
        let mut test = Test::new();

        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));
        test.next().await;
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.next().await;
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
        test.next().await;
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
        test.enqueue(TestMedia::Video2s);
        test.next().await;
        assert_eq!(test.karapulse.state(), State::Playing);
        test.next().await;
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
        test.pause().await;
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Playing));
        test.next().await;
        assert_eq!(test.karapulse.state(), State::Waiting);

        test.enqueue(TestMedia::Video2s);
        test.enqueue(TestMedia::Video2s);
        assert_eq!(test.karapulse.state(), State::Playing);
        test.pause().await;
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Playing));
        test.next().await;
        assert_eq!(test.karapulse.state(), State::Playing);
        test.next().await;
        assert_eq!(test.karapulse.state(), State::Waiting);
    }

    async fn announce() {
        println!("announce");
        let mut test = Test::new();

        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));
        test.enqueue_song(TestMedia::Video2s, "artist1", "title1");
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));

        // play; announce is displayed then video
        test.play().await;
        assert_eq!(test.karapulse.state(), State::Announcing);
        test.wait_while(State::Announcing);
        assert_eq!(test.karapulse.state(), State::Playing);
        test.wait_while(State::Playing);
        assert_eq!(test.karapulse.state(), State::Waiting);
    }

    async fn announce_pause() {
        println!("announce_pause");
        let mut test = Test::new();

        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));
        test.enqueue_song(TestMedia::Video2s, "artist1", "title1");
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));

        test.play().await;
        // announce is displayed
        assert_eq!(test.karapulse.state(), State::Announcing);

        // pause then wait the announcing time, ensuring it's actually paused
        test.pause().await;
        assert_eq!(
            test.karapulse.state(),
            State::Paused(PausedState::Announcing)
        );
        thread::sleep(time::Duration::from_secs(ANNOUNCE_DURATION + 1));
        assert_eq!(
            test.karapulse.state(),
            State::Paused(PausedState::Announcing)
        );

        // resume
        test.play().await;
        assert_eq!(test.karapulse.state(), State::Announcing);
        test.wait_while(State::Announcing);
        assert_eq!(test.karapulse.state(), State::Playing);
    }

    async fn announce_next() {
        println!("announce_next");
        let mut test = Test::new();

        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));
        test.enqueue_song(TestMedia::Video2s, "artist1", "title1");
        test.enqueue_song(TestMedia::Video2s, "artist2", "title2");
        assert_eq!(test.karapulse.state(), State::Paused(PausedState::Waiting));

        test.play().await;
        // first announce is displayed
        assert_eq!(test.karapulse.state(), State::Announcing);

        // next, second announce is displayed
        test.next().await;
        assert_eq!(test.karapulse.state(), State::Announcing);

        test.wait_while(State::Announcing);
        assert_eq!(test.karapulse.state(), State::Playing);
    }
}
