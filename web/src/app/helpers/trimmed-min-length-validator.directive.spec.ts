import {trimmedMinLength, TrimmedMinLengthValidatorDirective} from './trimmed-min-length-validator.directive';
import {FormControl} from "@angular/forms";



describe('TrimmedMinLengthDirective', () => {
  it('should create an instance', () => {
    const directive = new TrimmedMinLengthValidatorDirective();
    expect(directive).toBeTruthy();
  });

  it('validate 1 char + space as false', () => {
    expect(trimmedMinLength(3)(new FormControl("1 "))).toEqual({
      'minlength': {'valid': false}
    });
  });
  it('validate `Olivier` as true', () => {
    expect(trimmedMinLength(3)(new FormControl('Olivier'))).toEqual(null);

  });
  it('validate `ABCD123 ` as true', () => {
    expect(trimmedMinLength(3)(new FormControl(" ABCD123 "))).toEqual(null);
  });

  it('validate 1 char + space on both side as false', () => {
    expect(trimmedMinLength(3)(new FormControl(" a "))).toEqual({
      'minlength': {'valid': false}
    });
  });
});

