export type Song  = {
  id: string;
  artist: string;
  title: string;
  length: number;
  cover?: string; // Cover url
}
