import { Component } from '@angular/core';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.css']
})
export class QRCodeComponent {
  public JoinQrCode: string;
  constructor (private location: Location) {
    // assign a value
    this.JoinQrCode = window.location.origin;
  }
}
