import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {QueueComponent} from './queue.component';
import {BrowserModule} from "@angular/platform-browser";
import {AppRoutingModule} from "../app-routing.module";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpClientModule} from "@angular/common/http";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatMenuModule} from "@angular/material/menu";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {FormsModule} from "@angular/forms";
import {MatTableModule} from "@angular/material/table";
import {MatListModule} from "@angular/material/list";
import {MAT_BOTTOM_SHEET_DATA, MatBottomSheetModule, MatBottomSheetRef} from "@angular/material/bottom-sheet";
import {MatDialogModule} from "@angular/material/dialog";
import {MatTabsModule} from "@angular/material/tabs";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {QueueEntryComponent} from "../queue-entry/queue-entry.component";

describe('QueueComponent', () => {
  let component: QueueComponent;
  let fixture: ComponentFixture<QueueComponent>;
    const mockBottomSheetRef = {
    dismiss: jasmine.createSpy('dismiss')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatToolbarModule,
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        MatInputModule,
        FormsModule,
        MatTableModule,
        MatListModule,
        MatBottomSheetModule,
        MatDialogModule,
        MatTabsModule,
        MatProgressSpinnerModule,
      ],
      declarations: [QueueComponent],

      providers: [
        {
          provide: MatBottomSheetRef,
          useValue: mockBottomSheetRef
        },
        {
          provide: MAT_BOTTOM_SHEET_DATA,
          useValue: QueueEntryComponent,
        },
    ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should display eta properly", () => {
    expect(component.etaAsStr(20)).toBe('0:20');
    expect(component.etaAsStr(60)).toBe('1:00');
    expect(component.etaAsStr(61)).toBe('1:01');
    expect(component.etaAsStr(91)).toBe('1:31');
    expect(component.etaAsStr(71)).toBe('1:11');
    expect(component.etaAsStr(3600)).toBe('1:00:00');
    expect(component.etaAsStr(3602)).toBe('1:00:02');
    expect(component.etaAsStr(7712)).toBe('2:08:32');
  });
});
