import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ProfileComponent} from './profile/profile.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {SearchComponent} from './search/search.component';
import {QueueComponent} from './queue/queue.component';
import {HistoryComponent} from './history/history.component';
import {QRCodeComponent} from './qrcode/qrcode.component';
import {ConfigPageComponent} from "./config-page/config-page.component";

const routes: Routes = [
  { path: 'profile', component: ProfileComponent },
  {
    path: 'dashboard', component: DashboardComponent, children: [
      {path: 'search', component: SearchComponent},
      {path: 'queue', component: QueueComponent},
      {path: 'history', component: HistoryComponent},
    ]
  },
  { path: '', redirectTo: 'dashboard/search', pathMatch: 'full' },
  { path: 'qrcode', component: QRCodeComponent },
  { path: 'config', component: ConfigPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
