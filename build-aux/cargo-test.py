#!/usr/bin/env python3

import sys
import subprocess
import os

env = os.environ

(MESON_BUILD_ROOT, MESON_SOURCE_ROOT, CARGO_PROFILE, FEATURES, OFFLINE) = sys.argv[1:]

CARGO_TARGET_DIR = os.path.join(MESON_BUILD_ROOT, "target")
env["CARGO_TARGET_DIR"] = CARGO_TARGET_DIR

if CARGO_PROFILE == "Devel":
    RELEASE = "--verbose"
else:
    RELEASE = "--release"

CMD = [
    "cargo",
    "test",
    "--no-fail-fast",
    OFFLINE,
    RELEASE,
    "--manifest-path",
    os.path.join(MESON_SOURCE_ROOT, "Cargo.toml"),
]

if len(FEATURES) > 0:
    CMD += ["--features", FEATURES]

subprocess.run(CMD, env=env)
